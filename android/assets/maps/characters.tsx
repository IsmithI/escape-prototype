<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="characters" tilewidth="32" tileheight="48" margin="1" tilecount="31" columns="31">
 <image source="../atlas/characters.png" width="1024" height="64"/>
 <tile id="0">
  <objectgroup draworder="index" id="2">
   <object id="1" name="Origin" x="16" y="42">
    <point/>
   </object>
  </objectgroup>
 </tile>
</tileset>
