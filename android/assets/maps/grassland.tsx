<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="grassland" tilewidth="32" tileheight="32" tilecount="144" columns="8">
 <image source="../atlas/grassland.png" width="256" height="576"/>
 <terraintypes>
  <terrain name="Grass" tile="25"/>
 </terraintypes>
 <tile id="16" terrain=",,,0"/>
 <tile id="17" terrain=",,0,0"/>
 <tile id="18" terrain=",,0,"/>
 <tile id="24" terrain=",0,,0"/>
 <tile id="25" terrain="0,0,0,0"/>
 <tile id="26" terrain="0,,0,"/>
 <tile id="27" terrain="0,0,0,"/>
 <tile id="28" terrain="0,0,,0"/>
 <tile id="32" terrain=",0,,"/>
 <tile id="33" terrain="0,0,,"/>
 <tile id="34" terrain="0,,,"/>
 <tile id="35" terrain="0,,0,0"/>
 <tile id="36" terrain=",0,0,0"/>
 <tile id="73">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="0" width="16" height="24"/>
  </objectgroup>
 </tile>
 <tile id="74">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="0" width="8" height="24"/>
  </objectgroup>
 </tile>
</tileset>
