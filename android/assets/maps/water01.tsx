<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="water01" tilewidth="32" tileheight="32" tilecount="72" columns="12">
 <image source="../atlas/water01.png" width="384" height="192"/>
 <terraintypes>
  <terrain name="Water" tile="49"/>
 </terraintypes>
 <tile id="0" terrain="0,0,0,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="120"/>
   <frame tileid="3" duration="120"/>
   <frame tileid="6" duration="120"/>
   <frame tileid="9" duration="120"/>
  </animation>
 </tile>
 <tile id="1" terrain="0,0,,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="12" terrain="0,,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
  <animation>
   <frame tileid="12" duration="120"/>
   <frame tileid="15" duration="120"/>
   <frame tileid="18" duration="120"/>
   <frame tileid="21" duration="120"/>
  </animation>
 </tile>
 <tile id="13" terrain=",0,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
  <animation>
   <frame tileid="13" duration="120"/>
   <frame tileid="16" duration="120"/>
   <frame tileid="19" duration="120"/>
   <frame tileid="22" duration="120"/>
  </animation>
 </tile>
 <tile id="36" terrain=",,,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8" y="8" width="24" height="24"/>
  </objectgroup>
  <animation>
   <frame tileid="36" duration="120"/>
   <frame tileid="39" duration="120"/>
   <frame tileid="42" duration="120"/>
   <frame tileid="45" duration="120"/>
  </animation>
 </tile>
 <tile id="37" terrain=",,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8" width="32" height="24"/>
  </objectgroup>
  <animation>
   <frame tileid="37" duration="120"/>
   <frame tileid="40" duration="120"/>
   <frame tileid="43" duration="120"/>
   <frame tileid="46" duration="120"/>
  </animation>
 </tile>
 <tile id="38" terrain=",,0,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8" width="24" height="24"/>
  </objectgroup>
  <animation>
   <frame tileid="38" duration="120"/>
   <frame tileid="41" duration="120"/>
   <frame tileid="44" duration="120"/>
   <frame tileid="47" duration="120"/>
  </animation>
 </tile>
 <tile id="48" terrain=",0,,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8" y="0" width="24" height="32"/>
  </objectgroup>
  <animation>
   <frame tileid="48" duration="120"/>
   <frame tileid="51" duration="120"/>
   <frame tileid="54" duration="120"/>
   <frame tileid="57" duration="120"/>
  </animation>
 </tile>
 <tile id="49" terrain="0,0,0,0"/>
 <tile id="50" terrain="0,,0,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="24" height="32"/>
  </objectgroup>
  <animation>
   <frame tileid="50" duration="120"/>
   <frame tileid="53" duration="120"/>
   <frame tileid="56" duration="120"/>
   <frame tileid="59" duration="120"/>
  </animation>
 </tile>
 <tile id="60" terrain=",0,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8" y="0" width="24" height="16"/>
  </objectgroup>
  <animation>
   <frame tileid="60" duration="120"/>
   <frame tileid="63" duration="120"/>
   <frame tileid="66" duration="120"/>
   <frame tileid="69" duration="120"/>
  </animation>
 </tile>
 <tile id="61" terrain="0,0,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="16"/>
  </objectgroup>
  <animation>
   <frame tileid="61" duration="120"/>
   <frame tileid="64" duration="120"/>
   <frame tileid="67" duration="120"/>
   <frame tileid="70" duration="120"/>
  </animation>
 </tile>
 <tile id="62" terrain="0,,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="24" height="16"/>
  </objectgroup>
  <animation>
   <frame tileid="62" duration="120"/>
   <frame tileid="65" duration="120"/>
   <frame tileid="68" duration="120"/>
   <frame tileid="71" duration="120"/>
  </animation>
 </tile>
</tileset>
