<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="environment" tilewidth="160" tileheight="160" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="117" height="158" source="sprites/tree01.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="51.0452" y="127.874" width="21.0334" height="18.9088"/>
   <object id="3" name="Origin" x="61.375" y="129.5">
    <point/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <image width="160" height="160" source="sprites/tent01.png"/>
  <objectgroup draworder="index" id="3">
   <object id="5" x="17.8936" y="59.4726" width="124.741" height="94.2134"/>
   <object id="6" name="Origin" x="79.75" y="59.75">
    <point/>
   </object>
  </objectgroup>
 </tile>
</tileset>
