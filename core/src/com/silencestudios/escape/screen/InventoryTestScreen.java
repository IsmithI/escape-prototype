package com.silencestudios.escape.screen;

import com.badlogic.gdx.assets.AssetManager;
import com.silencestudios.escape.entity.ui.grid.UIItemData;
import com.silencestudios.escape.entity.ui.inventory.UIInventory;
import com.silencestudios.escape.ui.UIFactory;
import com.silencestudios.gdxengine.EngineGame;
import com.silencestudios.gdxengine.config.ScreenConfig;
import com.silencestudios.gdxengine.instance.Instance;
import com.silencestudios.gdxengine.screen.BaseScreen;

public class InventoryTestScreen extends BaseScreen {

    public InventoryTestScreen(AssetManager assetManager, ScreenConfig screenConfig, EngineGame game) {
        super(assetManager, screenConfig, game);
    }

    @Override
    protected void configureEngine() {
        Instance.get().create(UIFactory.class);

        UIInventory inventory = Instance.get().create(UIInventory.class);
        inventory.addItem(UIItemData.SHORTSWORD);
        inventory.addItem(UIItemData.SHORTSWORD);
        inventory.toggleVisibility();
    }
}
