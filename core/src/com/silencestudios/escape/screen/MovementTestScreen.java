package com.silencestudios.escape.screen;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.assets.AssetManager;
import com.silencestudios.escape.entity.Character;
import com.silencestudios.escape.entity.Tree;
import com.silencestudios.escape.entity.item.DroppedItemsFactory;
import com.silencestudios.escape.entity.map.Map;
import com.silencestudios.escape.entity.map.factories.DroppedItemMapObjectFactory;
import com.silencestudios.escape.entity.ui.global.menu.GlobalMenu;
import com.silencestudios.escape.entity.ui.hotbar.UIHotbar;
import com.silencestudios.escape.entity.ui.inventory.InventoryData;
import com.silencestudios.escape.entity.ui.inventory.UIInventory;
import com.silencestudios.escape.system.CameraFollowSystem;
import com.silencestudios.escape.system.GamePausingSystem;
import com.silencestudios.escape.system.InventorySystem;
import com.silencestudios.escape.system.ItemsPickSystem;
import com.silencestudios.escape.system.RemoveTimeoutSystem;
import com.silencestudios.escape.system.TargetMovingSystem;
import com.silencestudios.escape.system.ZIndexSystem;
import com.silencestudios.escape.system.player.animation.PlayerAnimationSystem;
import com.silencestudios.escape.system.player.combat.PlayerCombatSystem;
import com.silencestudios.escape.system.player.movement.PlayerMovementSystem;
import com.silencestudios.escape.ui.UIFactory;
import com.silencestudios.gdxengine.EngineGame;
import com.silencestudios.gdxengine.config.ScreenConfig;
import com.silencestudios.gdxengine.instance.Instance;
import com.silencestudios.gdxengine.screen.BaseScreen;
import com.silencestudios.gdxengine.system.MapRendererSystem;
import com.silencestudios.gdxengine.system.tiled.MapObjects;
import com.silencestudios.gdxengine.system.tiled.TiledMapFactories;
import com.silencestudios.gdxengine.system.tiled.TiledMapSystem;

import java.util.HashMap;

public class MovementTestScreen extends BaseScreen {

    public MovementTestScreen(AssetManager assetManager, ScreenConfig screenConfig, EngineGame game) {
        super(assetManager, screenConfig, game);
    }

    @Override
    protected void configureEngine() {
        HashMap<String, Class<? extends Entity>> entities = MapObjects.get().entities;
        entities.put("Player", Character.class);
        entities.put("Tree", Tree.class);

        TiledMapFactories.get().put("DroppedItem", new DroppedItemMapObjectFactory());

        addSystem(GamePausingSystem.class);
        addSystem(ZIndexSystem.class);
        addSystem(MapRendererSystem.class);
        addSystem(TiledMapSystem.class);

        Instance.get().create(UIFactory.class);
        Instance.get().create(GlobalMenu.class);
        Instance.get().create(DroppedItemsFactory.class);
        Instance.get().create(Map.class);

        addSystem(InventorySystem.class);
        addSystem(TargetMovingSystem.class);
        addSystem(RemoveTimeoutSystem.class);
        addSystem(PlayerMovementSystem.class);
        addSystem(PlayerAnimationSystem.class);
        addSystem(PlayerCombatSystem.class);
        addSystem(ItemsPickSystem.class);
        addSystem(CameraFollowSystem.class);
    }
}
