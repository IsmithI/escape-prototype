package com.silencestudios.escape.entity.map;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.silencestudios.escape.entity.Layer;
import com.silencestudios.escape.entity.item.DroppedItemsFactory;
import com.silencestudios.gdxengine.component.MapRenderer;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.component.annotations.Resource;

public class Map extends Entity {

    @Resource(key = "maps/level1.tmx")
    private TiledMap tiledMap;

    @Dependency
    private DroppedItemsFactory droppedItemsFactory;

    @ComponentProvider
    private Transform transform;

    @ComponentProvider
    private MapRenderer mapRenderer() {
        transform.setZ(Layer.LAND);
        return new MapRenderer(tiledMap);
    }
}
