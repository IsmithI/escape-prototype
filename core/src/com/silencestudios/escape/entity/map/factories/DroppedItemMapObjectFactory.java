package com.silencestudios.escape.entity.map.factories;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.silencestudios.escape.components.DroppedItemData;
import com.silencestudios.gdxengine.component.PhysicsBody;
import com.silencestudios.gdxengine.system.tiled.factories.TiledObjectFactory;

public class DroppedItemMapObjectFactory extends TiledObjectFactory {

    @Override
    public Entity make(MapObject mapObject, MapLayer mapLayer, Entity map) {
        Entity droppedItem = super.make(mapObject, mapLayer, map);
        MapProperties properties = mapObject.getProperties();

        PhysicsBody physicsBody = new PhysicsBody();
        physicsBody.bodyDef.type = BodyDef.BodyType.StaticBody;

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(0.25f, 0.25f);

        physicsBody.fixtureDef.shape = polygonShape;
        physicsBody.fixtureDef.isSensor = true;

        DroppedItemData droppedItemData = new DroppedItemData();
        droppedItemData.name = mapObject.getName();
        droppedItemData.sprite = (String) properties.get("sprite");
        droppedItemData.count = properties.containsKey("count") ? (int) properties.get("count") : 1;

        droppedItem.add(droppedItemData);
        droppedItem.add(physicsBody);

        return droppedItem;
    }
}
