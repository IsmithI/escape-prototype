package com.silencestudios.escape.entity.map;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.silencestudios.escape.entity.Layer;
import com.silencestudios.gdxengine.component.RepeatedSpriteDrawer;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Resource;

public class Grass extends Entity {

    @Resource(key = "atlas/grassland.atlas")
    private TextureAtlas textureAtlas;

    @ComponentProvider
    private Transform transform() {
        Transform transform = new Transform();
        transform.setZ(Layer.LAND);
        return transform;
    }

    @ComponentProvider
    private RepeatedSpriteDrawer repeatedSpriteDrawer() {
        return new RepeatedSpriteDrawer(textureAtlas.createSprite("grass-tile"), 64, 64);
    }
}
