package com.silencestudios.escape.entity;

public class Layer {
    public static final int LAND = -999999;
    public static final int ENVIRONMENT = 0;
    public static final int CHARACTERS = 1;
    public static final int ITEM_EFFECTS = 2;
}
