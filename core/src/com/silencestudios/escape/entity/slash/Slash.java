package com.silencestudios.escape.entity.slash;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.silencestudios.gdxengine.component.Animator;
import com.silencestudios.gdxengine.component.PhysicsBody;
import com.silencestudios.gdxengine.component.Shader;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Resource;
import com.silencestudios.gdxengine.instance.Instance;
import com.silencestudios.gdxengine.utils.AnimationBuilder;

import java.util.HashMap;

public class Slash extends Entity {

    @Resource(key = "atlas/slash01.atlas")
    private TextureAtlas textureAtlas;

    @ComponentProvider
    private Transform transform() {
        Transform transform = new Transform();
        transform.setOffset(0, 0.5f);
        return transform;
    }

    @ComponentProvider
    private Shader shader() {
        ShaderProgram shaderProgram = new ShaderProgram(
                Gdx.files.internal("shaders/default.vert"),
                Gdx.files.internal("shaders/tint.frag")
        );
        if (!shaderProgram.isCompiled()) {
            Gdx.app.log("Slash", shaderProgram.getLog());
            Gdx.app.exit();
        }

        Shader shader = new Shader(shaderProgram);
        shader.uniforms.put("u_targetColor", Color.valueOf("#e83110"));
        return shader;
    }

    @ComponentProvider
    private PhysicsBody physicsBody() {
        PhysicsBody physicsBody = new PhysicsBody();

        physicsBody.bodyDef.type = BodyDef.BodyType.DynamicBody;

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(0.5f, 0.5f);

        physicsBody.fixtureDef.shape = polygonShape;
        physicsBody.fixtureDef.isSensor = true;

        return physicsBody;
    }

    @ComponentProvider
    private Animator animator() {
        AnimationBuilder builder = new AnimationBuilder(textureAtlas);

        Animation<TextureRegion> slash01Animation = builder.useRegion("slash01")
                .splitBy(50, 50, 5, 1)
                .frameDuration(0.03f)
                .withPlayMode(Animation.PlayMode.NORMAL)
                .build();

        HashMap<String, Animation<TextureRegion>> animationHashMap = new HashMap<>();
        animationHashMap.put("slash01", slash01Animation);

        Animator animator = new Animator(animationHashMap);
        animator.setCurrentAnimation("slash01");
        animator.onAnimationEnd(() -> {
            Instance.get().remove(this);
        });

        return animator;
    }
}
