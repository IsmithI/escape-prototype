package com.silencestudios.escape.entity;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.silencestudios.gdxengine.component.PhysicsBody;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Resource;
import com.silencestudios.gdxengine.component.SpriteRenderer;
import com.silencestudios.gdxengine.component.Transform;

public class Tree extends Entity {

    @Resource(key = "atlas/environment.atlas")
    private TextureAtlas textureAtlas;

    @ComponentProvider
    private Transform transform;

    @ComponentProvider
    private SpriteRenderer spriteRenderer() {
        SpriteRenderer spriteRenderer = new SpriteRenderer(textureAtlas.createSprite("tree01"));
        spriteRenderer.getOrigin().y = 1f;
        spriteRenderer.getOrigin().x = 1.9f;
        return spriteRenderer;
    }

    @ComponentProvider
    private PhysicsBody physicsBody() {
        PhysicsBody physicsBody = new PhysicsBody();

        physicsBody.bodyDef.type = BodyDef.BodyType.StaticBody;

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(0.25f, 0.25f);

        physicsBody.fixtureDef.shape = polygonShape;

        return physicsBody;
    }
}
