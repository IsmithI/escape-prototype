package com.silencestudios.escape.entity.item;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.silencestudios.escape.components.DroppedItemData;
import com.silencestudios.gdxengine.component.PhysicsBody;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Resource;
import com.silencestudios.gdxengine.component.Transform;

public class DroppedItem extends Entity {

    @Resource(key = "atlas/items.atlas")
    private TextureAtlas textureAtlas;

    @ComponentProvider
    private DroppedItemData droppedItemData;

    @ComponentProvider
    public Transform transform;

    @ComponentProvider
    private PhysicsBody physicsBody() {
        PhysicsBody physicsBody = new PhysicsBody();

        physicsBody.bodyDef.type = BodyDef.BodyType.StaticBody;

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(0.25f, 0.25f);

        physicsBody.fixtureDef.shape = polygonShape;
        physicsBody.fixtureDef.isSensor = true;

        return physicsBody;
    }
}
