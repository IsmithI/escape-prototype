package com.silencestudios.escape.entity.item;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.silencestudios.escape.components.TargetMoving;
import com.silencestudios.escape.entity.Layer;
import com.silencestudios.gdxengine.component.Lifecycle;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.instance.Instance;

public class PickedItemEffect extends Entity {

    private float time;
    private Transform transform;

    @ComponentProvider
    private Transform transform() {
        transform = new Transform();
        transform.setZ(Layer.ITEM_EFFECTS);
        return transform;
    }

    @ComponentProvider
    private TargetMoving targetMoving() {
        TargetMoving targetMoving = new TargetMoving();
        targetMoving.speed = 3f;
        targetMoving.OnTargetReached = () -> Instance.get().remove(this);

        return targetMoving;
    }

    @ComponentProvider
    private Lifecycle lifecycle() {
        Lifecycle lifecycle = new Lifecycle();

        Vector2 scale = new Vector2();
        lifecycle.onUpdate = dt -> {
            float value = Interpolation.swingOut.apply(1f, 0f, time);
            transform.setScale(scale.set(value, value));

            time += dt * 2f;
        };

        return lifecycle;
    }

}
