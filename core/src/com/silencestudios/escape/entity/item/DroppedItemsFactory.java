package com.silencestudios.escape.entity.item;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.silencestudios.escape.components.DroppedItemData;
import com.silencestudios.escape.entity.Layer;
import com.silencestudios.gdxengine.component.SpriteRenderer;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.component.annotations.Resource;
import com.silencestudios.gdxengine.instance.Instance;

@Dependency
public class DroppedItemsFactory extends Entity {

    private static DroppedItemsFactory instance;

    public static DroppedItemsFactory get() {
        if (instance == null) {
            instance = Instance.get().create(DroppedItemsFactory.class);
        }
        return instance;
    }

    @Resource(key = "atlas/items.atlas")
    private TextureAtlas textureAtlas;

    public DroppedItem makeItem(DroppedItemData data) {
        DroppedItem droppedItem = Instance.get().create(DroppedItem.class);

        SpriteRenderer spriteRenderer = droppedItem.getComponent(SpriteRenderer.class);
        spriteRenderer.setSprite(textureAtlas.createSprite(data.sprite));

        droppedItem.add(data);
        droppedItem.transform.setZ(Layer.ENVIRONMENT);

        return droppedItem;
    }
}
