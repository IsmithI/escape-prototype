package com.silencestudios.escape.entity;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.silencestudios.escape.ui.UIFactory;
import com.silencestudios.gdxengine.component.ActorContainer;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Dependency;

public class UITouchpad extends Entity {

    private Touchpad touchpad;

    @Dependency
    private UIFactory uiFactory;

    @ComponentProvider
    private Transform transform;

    @ComponentProvider
    private ActorContainer actorContainer() {
        touchpad = uiFactory.makeTouchpad();
        touchpad.moveBy(32f, 32f);
        return new ActorContainer(touchpad);
    }

    public Touchpad getTouchpad() {
        return touchpad;
    }
}
