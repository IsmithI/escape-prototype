package com.silencestudios.escape.entity;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.silencestudios.escape.components.Movable;
import com.silencestudios.escape.entity.player.PlayerItemsPicker;
import com.silencestudios.escape.entity.player.PlayerSprite;
import com.silencestudios.escape.entity.player.PlayerTag;
import com.silencestudios.gdxengine.component.Children;
import com.silencestudios.gdxengine.component.EntityNode;
import com.silencestudios.gdxengine.component.PhysicsBody;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Dependency;

@Dependency
public class Character extends Entity {

    @ComponentProvider
    private PlayerTag playerTag;

    @ComponentProvider
    private Movable movable() {
        Movable movable = new Movable();
        movable.speed = 2f;
        return movable;
    }

    @ComponentProvider
    private Transform transform() {
        Transform transform = new Transform();
        transform.setZ(Layer.CHARACTERS);
        return transform;
    }

    @ComponentProvider
    private PhysicsBody physicsBody() {
        PhysicsBody physicsBody = new PhysicsBody();

        physicsBody.bodyDef.type = BodyDef.BodyType.DynamicBody;
        physicsBody.bodyDef.fixedRotation = true;

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(0.25f, 0.25f);

        physicsBody.fixtureDef.shape = polygonShape;

        return physicsBody;
    }

    @ComponentProvider
    private Children children() {
        EntityNode.EntityNodeBuilder builder = new EntityNode.EntityNodeBuilder();
        return new Children(
                builder.className(PlayerSprite.class)
                        .setZ(Layer.CHARACTERS)
                        .moveBy(0, 0.5f)
                        .build(),
                builder.className(PlayerItemsPicker.class)
                        .setZ(Layer.CHARACTERS)
                        .moveBy(0, 0.5f)
                        .build()
        );
    }
}
