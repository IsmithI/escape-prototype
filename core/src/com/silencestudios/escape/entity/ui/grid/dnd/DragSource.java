package com.silencestudios.escape.entity.ui.grid.dnd;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.ObjectMap;
import com.silencestudios.escape.entity.ui.cell.UIInventoryCell;

public class DragSource extends DragAndDrop.Source {

    private DragAndDrop.Payload payload = new DragAndDrop.Payload();
    private int index;
    private UIInventoryCell cell;

    public DragSource(int index, UIInventoryCell cell) {
        super(cell.getActor());

        this.index = index;
        this.cell = cell;
    }

    @Override
    public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
        ObjectMap.Entry<Integer, UIInventoryCell> entry = new ObjectMap.Entry<>();
        entry.key = index;
        entry.value = cell;

        Actor actor = cell.detachItemActor();

        payload.setObject(entry);
        payload.setDragActor(actor);

        return payload;
    }

    @Override
    public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload, DragAndDrop.Target target) {
        if (target == null)
            cell.restoreItemActor();
    }
}
