package com.silencestudios.escape.entity.ui.grid;

import com.silencestudios.escape.entity.ui.inventory.InventoryData;

import java.util.Observable;

public class UIInventoryGridModel extends Observable {

    public int width = 5;
    public int height = 5;

    private boolean visible = false;
    private InventoryData inventory;

    public UIInventoryGridModel(InventoryData inventory) {
        this.inventory = inventory;
        inventory.addObserver((observable, o) -> {
            setChanged();
            notifyObservers();
        });
    }

    public void toggleVisible() {
        visible = !visible;
        setChanged();
        notifyObservers();
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        setChanged();
        notifyObservers();
    }

    public InventoryData getInventory() {
        return inventory;
    }
}
