package com.silencestudios.escape.entity.ui.grid;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Align;
import com.silencestudios.escape.entity.ui.cell.UIInventoryCell;
import com.silencestudios.escape.entity.ui.grid.dnd.DragSource;
import com.silencestudios.escape.entity.ui.grid.dnd.DragTarget;
import com.silencestudios.escape.entity.ui.item.UIInventoryItemModel;
import com.silencestudios.escape.ui.UIWidget;

import java.util.ArrayList;
import java.util.List;

public class UIInventoryGrid extends UIWidget<UIInventoryGridModel> {

    private boolean isShown = false;

    private Table table;
    private List<UIInventoryItemModel> cellModels = new ArrayList<>();

    public UIInventoryGrid(UIInventoryGridModel model) {
        super(model);

        table = new Table();
        table.setTransform(true);
        table.setVisible(model.isVisible());

        DragAndDrop dragAndDrop = new DragAndDrop();
        dragAndDrop.setDragActorPosition(16f, -16f);

        for (int i = 0; i < model.height; i++) {
            for (int j = 0; j < model.width; j++) {
                int index = j + i * model.width;

                UIItemData itemData = model.getInventory().get(index);

                UIInventoryItemModel cellModel = new UIInventoryItemModel();
                if (itemData != null) {
                    cellModel.setCount(itemData.getCount());
                    cellModel.setImage(itemData.getSprite());
                    cellModel.setName(itemData.getName());
                }

                UIInventoryCell cell = new UIInventoryCell(cellModel);

                table.add(cell.getActor()).center().pad(2f);

                cellModels.add(cellModel);

                dragAndDrop.addSource(new DragSource(index, cell));
                dragAndDrop.addTarget(new DragTarget(index, cell, model.getInventory()));
            }
            table.row();
        }

    }

    @Override
    public void update() {

        if (isShown != model.isVisible()) {
            if (model.isVisible()) show();
            else hide();

            isShown = model.isVisible();
        }

        int i = 0;
        for (UIItemData itemData : model.getInventory().getItems()) {
            UIInventoryItemModel cellModel = cellModels.get(i);
            cellModel.setFrom(itemData);
            i++;
        }
    }

    @Override
    public Actor getActor() {
        return table;
    }

    private void show() {
        int i = 0;
        table.setVisible(true);
        table.getColor().a = 0;
        table.addAction(Actions.fadeIn(0.2f));
        for (Actor actor : table.getChildren()) {
            actor.setOrigin(Align.center);
            actor.addAction(Actions.sequence(
                    Actions.scaleTo(0, 0),
                    Actions.delay(i * 0.01f, Actions.scaleTo(1, 1, 0.2f, Interpolation.swingOut))
            ));

            i++;
        }
    }

    private void hide() {
        int i = 0;
        float delay = (table.getChildren().size) * 0.01f;
        for (Actor actor : table.getChildren()) {
            actor.setOrigin(Align.center);
            actor.addAction(Actions.sequence(
                    Actions.scaleTo(1, 1),
                    Actions.delay(i * 0.01f, Actions.scaleTo(0, 0, 0.2f, Interpolation.swingIn))
            ));
            i++;
        }

        table.getColor().a = 1;
        table.addAction(Actions.parallel(
                Actions.fadeOut(0.2f),
                Actions.delay(delay + 0.2f, Actions.run(() -> table.setVisible(false)))
        ));
    }
}
