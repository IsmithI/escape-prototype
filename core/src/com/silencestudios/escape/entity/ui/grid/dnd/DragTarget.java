package com.silencestudios.escape.entity.ui.grid.dnd;

import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.ObjectMap;
import com.silencestudios.escape.entity.ui.cell.UIInventoryCell;
import com.silencestudios.escape.entity.ui.inventory.InventoryData;

public class DragTarget extends DragAndDrop.Target {

    private int index;
    private InventoryData inventoryData;

    public DragTarget(int index, UIInventoryCell cell, InventoryData inventoryData) {
        super(cell.getActor());
        this.index = index;
        this.inventoryData = inventoryData;
    }

    @Override
    public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        return true;
    }

    @Override
    public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        ObjectMap.Entry<Integer, UIInventoryCell> entry = (ObjectMap.Entry<Integer, UIInventoryCell>) payload.getObject();
        entry.value.restoreItemActor();
        inventoryData.swapItems(entry.key, index);
    }

}
