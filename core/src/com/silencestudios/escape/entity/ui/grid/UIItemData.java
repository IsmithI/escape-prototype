package com.silencestudios.escape.entity.ui.grid;

public class UIItemData {

    private String name;
    private String sprite;
    private int count;

    public UIItemData() {
    }

    public UIItemData(String sprite) {
        this.setSprite(sprite);
    }

    public UIItemData(String sprite, int count) {
        this.setSprite(sprite);
        this.setCount(count);
    }

    public UIItemData(String name, String sprite, int count) {
        this.name = name;
        this.sprite = sprite;
        this.count = count;
    }

    public static final UIItemData SHORTSWORD = new UIItemData("Shortsword", "shortsword", 1);

    public String getSprite() {
        return sprite;
    }

    public void setSprite(String sprite) {
        this.sprite = sprite;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "UIItemData{" +
                "sprite='" + getSprite() + '\'' +
                ", count=" + getCount() +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
