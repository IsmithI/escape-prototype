package com.silencestudios.escape.entity.ui.inventory;

public class UIInventoryPanelModel extends InventoryData {

    private boolean visible = false;

    public UIInventoryPanelModel(int capacity) {
        super(capacity);
    }

    public void toggleVisibility() {
        visible = !visible;
        setChanged();
        notifyObservers();
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
        setChanged();
        notifyObservers();
    }
}
