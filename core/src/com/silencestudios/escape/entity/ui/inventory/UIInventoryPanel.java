package com.silencestudios.escape.entity.ui.inventory;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.silencestudios.escape.entity.ui.grid.UIInventoryGrid;
import com.silencestudios.escape.entity.ui.grid.UIInventoryGridModel;
import com.silencestudios.escape.ui.UIFactory;
import com.silencestudios.escape.ui.UIWidget;

public class UIInventoryPanel extends UIWidget<UIInventoryPanelModel> {

    private boolean visible;
    private Table table;
    private UIInventoryGrid inventoryGrid;

    public UIInventoryPanel(UIInventoryPanelModel model) {
        super(model);
        this.visible = model.isVisible();

        UIInventoryGridModel inventoryGridModel = new UIInventoryGridModel(model);
        inventoryGridModel.setVisible(model.isVisible());

        inventoryGrid = new UIInventoryGrid(inventoryGridModel);

        Label title = UIFactory.get().makeLabel("Inventory");

        table = UIFactory.get().makeTable(true);
        table.pad(4f);
        table.setVisible(model.isVisible());
        table.add(title).padLeft(4f).left().row();
        table.add(inventoryGrid.getActor());

        update();
    }

    @Override
    public void update() {
        if (visible != model.isVisible()) {
            if (model.isVisible()) show();
            else hide();

            visible = model.isVisible();
        }

        inventoryGrid.getModel().setVisible(model.isVisible());
    }

    @Override
    public Actor getActor() {
        return table;
    }

    private void show() {
        table.setVisible(true);
        table.getColor().a = 0;
        table.addAction(Actions.fadeIn(0.2f));
    }

    private void hide() {
        table.addAction(Actions.sequence(
                Actions.delay(0.25f, Actions.fadeOut(0.1f)),
                Actions.run(() -> table.setVisible(false))
        ));
    }
}
