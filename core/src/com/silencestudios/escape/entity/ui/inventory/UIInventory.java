package com.silencestudios.escape.entity.ui.inventory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.silencestudios.escape.entity.ui.grid.UIItemData;
import com.silencestudios.escape.entity.ui.hotbar.UIHotbar;
import com.silencestudios.escape.entity.ui.hotbar.UIHotbarModel;
import com.silencestudios.escape.ui.UIFactory;
import com.silencestudios.gdxengine.component.ActorContainer;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.utils.ActorUtils;

@Dependency
public class UIInventory extends Entity {

    private UIHotbarModel hotbarModel;
    private UIInventoryPanelModel inventoryPanelModel;

    @ComponentProvider
    private Transform transform;

    @ComponentProvider
    private ActorContainer actorContainer() {
        Stack root = new Stack();
        root.setFillParent(true);

        hotbarModel = new UIHotbarModel(new InventoryData(8));
        UIHotbar hotbar = new UIHotbar(hotbarModel);

        inventoryPanelModel = new UIInventoryPanelModel(25);
        UIInventoryPanel inventoryPanel = new UIInventoryPanel(inventoryPanelModel);

        Table inventoryRoot = new Table();
        inventoryRoot.setFillParent(true);
        inventoryRoot.top().right();
        inventoryRoot.add(inventoryPanel.getActor()).pad(8f);

        ImageButton backpackButton = UIFactory.get().makeImageButton("backpack");
        ActorUtils.onClick(backpackButton, inventoryPanelModel::toggleVisibility);

        Table table = new Table();
        table.setFillParent(true);
        table.bottom().right().pad(8f);
        table.add(backpackButton);

        root.add(table);
        root.add(inventoryRoot);
        root.add(hotbar.getActor());

        return new ActorContainer(root);
    }

    public void addItem(UIItemData itemData) {
        inventoryPanelModel.addItem(itemData);
    }

    public void toggleVisibility() {
        inventoryPanelModel.toggleVisibility();
    }
}
