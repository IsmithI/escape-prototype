package com.silencestudios.escape.entity.ui.inventory;

import com.silencestudios.escape.entity.ui.grid.UIItemData;

import java.util.Observable;

public class InventoryData extends Observable {

    private int capacity;
    private UIItemData[] items;

    public InventoryData(int capacity) {
        this.capacity = capacity;
        items = new UIItemData[capacity];

        for (int i = 0; i < capacity; i++) {
            items[i] = null;
        }
    }

    public UIItemData[] getItems() {
        return items;
    }

    public UIItemData get(int i) {
        return items[i];
    }

    public void addItem(UIItemData itemData) {
        for (UIItemData item : items) {
            if (item == null)
                continue;

            if (itemData.getName().equals(item.getName())) {
                item.setCount(item.getCount() + itemData.getCount());
                setChanged();
                notifyObservers();
                return;
            }
        }

        int i = 0;
        for (UIItemData item : items) {
            if (item == null) {
                items[i] = itemData;
                setChanged();
                notifyObservers();
                return;
            }

            i++;
        }
    }

    public void removeItem(UIItemData itemData) {
        int i = 0;
        for (UIItemData item : items) {
            if (item == null)
                continue;

            if (itemData.getName().equals(item.getName())) {
                item.setCount(item.getCount() - itemData.getCount());
                if (item.getCount() <= 0) {
                    items[i] = null;
                }

                setChanged();
                notifyObservers();
                return;
            }
        }
    }

    public void swapItems(int sourceI, int targetI) {
        UIItemData source = items[sourceI];
        UIItemData target = items[targetI];

        items[sourceI] = target;
        items[targetI] = source;

        setChanged();
        notifyObservers();
    }

    public int getCapacity() {
        return capacity;
    }
}
