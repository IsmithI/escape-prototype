package com.silencestudios.escape.entity.ui.item;

import com.silencestudios.escape.entity.ui.grid.UIItemData;

import java.util.Observable;

public class UIInventoryItemModel extends Observable {

    private String name;
    private String image;
    private int count;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
        setChanged();
        notifyObservers();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
        setChanged();
        notifyObservers();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        setChanged();
        notifyObservers();
    }

    public void setFrom(UIItemData itemData) {
        if (itemData == null) {
            this.name = null;
            this.count = 0;
            this.image = null;
        } else {
            this.name = itemData.getName();
            this.count = itemData.getCount();
            this.image = itemData.getSprite();
        }

        setChanged();
        notifyObservers();
    }

}
