package com.silencestudios.escape.entity.ui.item;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.utils.Align;
import com.silencestudios.escape.ui.UIFactory;
import com.silencestudios.escape.ui.UIWidget;

public class UIInventoryItem extends UIWidget<UIInventoryItemModel> {

    private final UIFactory uiFactory = UIFactory.get();

    private Stack stack;

    private int prevCount;

    private Image itemImage;
    private Label countLabel;
    private Container<Label> countLabelContainer;

    public UIInventoryItem(UIInventoryItemModel model) {
        super(model);

        stack = new Stack();
        stack.setTransform(true);

        itemImage = model.getImage() != null ?
                new Image(uiFactory.skin, model.getImage()) :
                new Image();
        countLabel = uiFactory.makeLabel(model.getCount() + "", "Small");
        if (getModel().getCount() <= 1) {
            countLabel.setVisible(false);
        }

        itemImage.setTouchable(Touchable.disabled);
        countLabel.setTouchable(Touchable.disabled);

        countLabelContainer = new Container<>(countLabel);
        countLabelContainer.setTransform(true);

        stack.add(new Container<>(countLabelContainer).align(Align.topRight).padRight(2f));
        stack.add(new Container<>(itemImage).minSize(24f));
    }

    @Override
    public void update() {
        if (itemImage.getDrawable() == null) {
            itemImage.setOrigin(Align.center);
            itemImage.addAction(Actions.sequence(
                    Actions.scaleTo(0, 0),
                    Actions.scaleTo(1f, 1f, 0.2f, Interpolation.swingOut)
            ));
        }

        if (model.getImage() != null)
            itemImage.setDrawable(uiFactory.skin, model.getImage());
        else
            itemImage.setDrawable(null);

        countLabel.setVisible(model.getCount() > 1);

        if (prevCount != model.getCount()) {
            countLabel.setText(model.getCount());
            countLabelContainer.clearActions();
            countLabelContainer.setScale(0);
            countLabelContainer.setOrigin(Align.center);
            countLabelContainer.addAction(Actions.sequence(
                    Actions.scaleTo(1f, 1f, 0.2f, Interpolation.swingOut)
            ));

            prevCount = model.getCount();
        }
    }

    @Override
    public Actor getActor() {
        return stack;
    }
}
