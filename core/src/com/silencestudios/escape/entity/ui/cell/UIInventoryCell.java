package com.silencestudios.escape.entity.ui.cell;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.silencestudios.escape.entity.ui.item.UIInventoryItem;
import com.silencestudios.escape.entity.ui.item.UIInventoryItemModel;
import com.silencestudios.escape.ui.UIFactory;
import com.silencestudios.escape.ui.UIWidget;

public class UIInventoryCell extends UIWidget {

    private final UIFactory uiFactory = UIFactory.get();

    private Stack stack;
    private UIInventoryItem inventoryItem;

    public UIInventoryCell(UIInventoryItemModel model) {

        stack = new Stack();

        Button button = uiFactory.makeButton();
        inventoryItem = new UIInventoryItem(model);

        stack.add(new Container<>(button).minSize(32f));
        stack.add(inventoryItem.getActor());
    }

    @Override
    public void update() {
    }

    @Override
    public Actor getActor() {
        return stack;
    }

    public Actor getItemActor() {
        return inventoryItem.getActor();
    }

    public Actor detachItemActor() {
        stack.removeActor(inventoryItem.getActor());
        stack.getStage().addActor(inventoryItem.getActor());

        return inventoryItem.getActor();
    }

    public void restoreItemActor() {
        stack.addActorAt(1, inventoryItem.getActor());
    }
}
