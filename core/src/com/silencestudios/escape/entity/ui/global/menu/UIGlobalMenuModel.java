package com.silencestudios.escape.entity.ui.global.menu;

import java.util.Observable;

public class UIGlobalMenuModel extends Observable {

    private boolean visible = false;

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        if (this.visible != visible) {
            this.visible = visible;
            setChanged();
        }
        notifyObservers();
    }

    public void toggleVisible() {
        this.setVisible(!isVisible());
    }
}
