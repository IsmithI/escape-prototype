package com.silencestudios.escape.entity.ui.global.menu;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.silencestudios.escape.ui.UIFactory;
import com.silencestudios.escape.ui.UIWidget;
import com.silencestudios.gdxengine.screen.EventDispatcher;
import com.silencestudios.gdxengine.utils.ActorUtils;

public class UIGlobalMenu extends UIWidget<UIGlobalMenuModel> {

    private Table table;
    private Table backdrop;

    public UIGlobalMenu(UIGlobalMenuModel model) {
        super(model);

        UIFactory uiFactory = UIFactory.get();
        EventDispatcher eventDispatcher = EventDispatcher.get();

        TextButton resumeButton = uiFactory.makeTextButton("Resume");
        TextButton exitButton = uiFactory.makeTextButton("Exit");

        ActorUtils.onClick(resumeButton, () -> {
            model.setVisible(false);
            eventDispatcher.dispatch("game.paused", false);
        });

        Container<TextButton> resumeButtonContainer = new Container<>(resumeButton);
        resumeButtonContainer.fill(true);
        resumeButtonContainer.setTransform(true);

        Container<TextButton> exitButtonContainer = new Container<>(exitButton);
        exitButtonContainer.fill();
        exitButtonContainer.setTransform(true);

        table = uiFactory.makeTable(true);
        table.setTransform(true);
        table.pad(8f);
        table.add(uiFactory.makeLabel("Pause menu")).center().pad(2f);
        table.row();
        table.add(resumeButtonContainer).expandX().fillX().pad(2f);
        table.row();
        table.add(exitButtonContainer).expandX().fillX().pad(2f);

        backdrop = new Table(uiFactory.skin);
        backdrop.setBackground("backdrop");
        backdrop.setVisible(model.isVisible());
        backdrop.setFillParent(true);
        backdrop.setTouchable(Touchable.enabled);
        backdrop.add(table).center();
        backdrop.layout();
    }

    @Override
    public void update() {
        if (model.isVisible()) {
            backdrop.setVisible(true);
            backdrop.getColor().a = 0;
            backdrop.clearActions();
            backdrop.addAction(Actions.fadeIn(0.25f));

            float delay = 0.025f;
            int i = 0;
            for (Actor child : table.getChildren()) {
                child.setOrigin(Align.center);
                child.clearActions();
                child.addAction(Actions.sequence(
                        Actions.scaleTo(0, 0),
                        Actions.delay(i * delay, Actions.scaleTo(1f, 1f, 0.3f, Interpolation.swingOut))
                ));

                i++;
            }
        } else {
            backdrop.clearActions();
            backdrop.addAction(Actions.sequence(
                    Actions.fadeOut(0.2f),
                    Actions.run(() -> backdrop.setVisible(false))
            ));
        }
    }

    @Override
    public Actor getActor() {
        return backdrop;
    }
}
