package com.silencestudios.escape.entity.ui.global.menu;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.silencestudios.gdxengine.component.ActorContainer;
import com.silencestudios.gdxengine.component.Lifecycle;
import com.silencestudios.gdxengine.component.Transform;;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.screen.EventDispatcher;

@Dependency
public class GlobalMenu extends Entity {

    private UIGlobalMenuModel globalMenuModel;

    @ComponentProvider
    private Transform transform;

    @ComponentProvider
    private ActorContainer actorContainer() {
        globalMenuModel = new UIGlobalMenuModel();
        UIGlobalMenu globalMenu = new UIGlobalMenu(globalMenuModel);

        return new ActorContainer(globalMenu.getActor());
    }

    @ComponentProvider
    private Lifecycle lifecycle() {
        Lifecycle lifecycle = new Lifecycle();

        lifecycle.onUpdate = dt -> {
            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
                globalMenuModel.toggleVisible();
                if (!globalMenuModel.isVisible()) {
                    System.out.println("game is resumed");
                    EventDispatcher.get().dispatch("game.paused", false);
                } else {
                    System.out.println("game is paused");
                    EventDispatcher.get().dispatch("game.paused", true);
                }
            }
        };

        return lifecycle;
    }
}
