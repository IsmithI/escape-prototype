package com.silencestudios.escape.entity.ui.hotbar;

import com.silencestudios.escape.entity.ui.inventory.InventoryData;

import java.util.Observable;

public class UIHotbarModel extends Observable {

    private InventoryData inventoryData;

    public UIHotbarModel(InventoryData inventoryData) {
        this.inventoryData = inventoryData;
    }

    public InventoryData getInventoryData() {
        return inventoryData;
    }
}
