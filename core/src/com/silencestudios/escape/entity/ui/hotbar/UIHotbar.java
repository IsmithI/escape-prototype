package com.silencestudios.escape.entity.ui.hotbar;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.silencestudios.escape.entity.ui.grid.UIInventoryGrid;
import com.silencestudios.escape.entity.ui.grid.UIInventoryGridModel;
import com.silencestudios.escape.ui.UIWidget;

public class UIHotbar extends UIWidget<UIHotbarModel> {

    private Table root;

    public UIHotbar(UIHotbarModel model) {
        super(model);

        root = new Table();
        root.setFillParent(true);
        root.bottom();
        root.pad(8f);

        UIInventoryGridModel inventoryGridModel = new UIInventoryGridModel(model.getInventoryData());
        inventoryGridModel.setVisible(true);
        inventoryGridModel.width = 8;
        inventoryGridModel.height = 1;

        UIInventoryGrid inventoryGrid = new UIInventoryGrid(inventoryGridModel);

        root.add(inventoryGrid.getActor()).expandX().center();
    }

    @Override
    public void update() {

    }

    @Override
    public Actor getActor() {
        return root;
    }
}
