package com.silencestudios.escape.entity;

import com.badlogic.ashley.core.Entity;

import java.util.HashMap;

public final class MapObjects {

    private static MapObjects instance = new MapObjects();

    public static MapObjects get() {
        return instance;
    }

    public final HashMap<String, Class<? extends Entity>> entities = new HashMap<>();

    public MapObjects() {
        entities.put("Player", Character.class);
        entities.put("Tree", Tree.class);
    }
}