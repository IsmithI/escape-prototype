package com.silencestudios.escape.entity.player;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.silencestudios.gdxengine.component.Animator;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Resource;
import com.silencestudios.gdxengine.utils.AnimationBuilder;

import java.util.HashMap;
import java.util.Map;

public class PlayerSprite extends Entity {

    @Resource(key = "atlas/characters.atlas")
    private TextureAtlas textureAtlas;

    @ComponentProvider
    private Transform transform;

    @ComponentProvider
    private Animator animator() {
        AnimationBuilder animationBuilder = new AnimationBuilder(textureAtlas);

        Animation<TextureRegion> runUpAnimation = animationBuilder.useRegion("001-Fighter04")
                .frameDuration(0.06f)
                .splitBy(32, 48, 4, 1)
                .withPlayMode(Animation.PlayMode.LOOP)
                .build();

        Animation<TextureRegion> runDownAnimation = animationBuilder.useRegion("001-Fighter01")
                .frameDuration(0.06f)
                .splitBy(32, 48, 4, 1)
                .withPlayMode(Animation.PlayMode.LOOP)
                .build();

        Animation<TextureRegion> runLeftAnimation = animationBuilder.useRegion("001-Fighter02")
                .frameDuration(0.06f)
                .splitBy(32, 48, 4, 1)
                .withPlayMode(Animation.PlayMode.LOOP)
                .build();

        Animation<TextureRegion> runRightAnimation = animationBuilder.useRegion("001-Fighter03")
                .frameDuration(0.06f)
                .splitBy(32, 48, 4, 1)
                .withPlayMode(Animation.PlayMode.LOOP)
                .build();

        Animation<TextureRegion> idleAnimation = animationBuilder.useRegion("001-Fighter03")
                .frameDuration(0.06f)
                .splitBy(32, 48, 1, 1)
                .withPlayMode(Animation.PlayMode.NORMAL)
                .build();

        Map<String, Animation<TextureRegion>> animationMap = new HashMap<>();
        animationMap.put("idle", idleAnimation);
        animationMap.put("run-up", runUpAnimation);
        animationMap.put("run-down", runDownAnimation);
        animationMap.put("run-left", runLeftAnimation);
        animationMap.put("run-right", runRightAnimation);

        Animator animator = new Animator(animationMap);
        animator.setCurrentAnimation("idle");

        return animator;
    }
}
