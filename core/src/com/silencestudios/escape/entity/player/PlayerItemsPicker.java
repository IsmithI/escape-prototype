package com.silencestudios.escape.entity.player;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.silencestudios.escape.components.ItemsPicker;
import com.silencestudios.escape.entity.ui.grid.UIItemData;
import com.silencestudios.escape.entity.ui.inventory.UIInventory;
import com.silencestudios.gdxengine.component.Lifecycle;
import com.silencestudios.gdxengine.component.PhysicsBody;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.component.annotations.ComponentProvider;
import com.silencestudios.gdxengine.component.annotations.Dependency;

public class PlayerItemsPicker extends Entity {

    @Dependency
    private UIInventory inventory;

    @ComponentProvider
    private Transform transform;

    @ComponentProvider
    private ItemsPicker itemsPicker;

    @ComponentProvider
    private PhysicsBody physicsBody() {
        PhysicsBody physicsBody = new PhysicsBody();

        physicsBody.bodyDef.type = BodyDef.BodyType.DynamicBody;
        physicsBody.bodyDef.allowSleep = false;

        CircleShape shape = new CircleShape();
        shape.setRadius(0.5f);

        physicsBody.fixtureDef.shape = shape;
        physicsBody.fixtureDef.isSensor = true;

        return physicsBody;
    }

    @ComponentProvider
    private Lifecycle lifecycle() {
        Lifecycle lifecycle = new Lifecycle();

        lifecycle.onCreate = () -> {
            itemsPicker.OnItemPicked = droppedItemData -> {
                UIItemData itemData = new UIItemData();
                itemData.setSprite(droppedItemData.sprite);
                itemData.setCount(droppedItemData.count);
                itemData.setName(droppedItemData.name);

                inventory.addItem(itemData);
            };
        };

        return lifecycle;
    }
}
