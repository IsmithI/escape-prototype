package com.silencestudios.escape;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.kotcrab.vis.ui.VisUI;
import com.silencestudios.escape.screen.InventoryTestScreen;
import com.silencestudios.escape.screen.MovementTestScreen;
import com.silencestudios.gdxeditor.screen.EditorScreen;
import com.silencestudios.gdxengine.EngineGame;
import com.silencestudios.gdxengine.component.SpriteRenderer;
import com.silencestudios.gdxengine.config.PhysicsConfig;
import com.silencestudios.gdxengine.config.ScreenConfig;
import com.silencestudios.gdxengine.entity.ObjectsLoader;
import com.silencestudios.gdxengine.scene.Scene;
import com.silencestudios.gdxengine.scene.SceneLoader;
import com.silencestudios.gdxengine.screen.BaseScreen;

public class GameMain extends EngineGame {

    @Override
    public void create() {
        if (!VisUI.isLoaded()) {
            VisUI.load();
        }

        AssetManager assetManager = new AssetManager();
        assetManager.setLoader(Scene.class, new SceneLoader());
        assetManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        assetManager.load("atlas/characters.png", Texture.class);
        assetManager.load("atlas/characters.atlas", TextureAtlas.class);
        assetManager.load("atlas/grassland.atlas", TextureAtlas.class);
        assetManager.load("atlas/environment.atlas", TextureAtlas.class);
        assetManager.load("atlas/slash01.atlas", TextureAtlas.class);
        assetManager.load("atlas/items.atlas", TextureAtlas.class);
        assetManager.load("maps/level1.tmx", TiledMap.class);
        assetManager.load("skin/skin.json", Skin.class);
        assetManager.finishLoading();

        ScreenConfig screenConfig = ScreenConfig.get();
        screenConfig.worldWidth = 16f;
        screenConfig.worldHeight = 16f / BaseScreen.getRatio();
        screenConfig.guiWidth = 600f;
        screenConfig.guiHeight = 600f / BaseScreen.getRatio();
        screenConfig.centerCamera = true;

        ObjectsLoader.get("com.silencestudios.escape");

        SpriteRenderer.SPRITE_SCALE = 32f;

        PhysicsConfig physicsConfig = PhysicsConfig.get();
        physicsConfig.setGravity(new Vector2(0, 0));
        physicsConfig.setDrawDebug(false);

        MovementTestScreen movementTestScreen = new MovementTestScreen(assetManager, screenConfig, this);
        InventoryTestScreen inventoryTestScreen = new InventoryTestScreen(assetManager, screenConfig, this);
        EditorScreen editorScreen = new EditorScreen(assetManager, screenConfig, this);

//        screens.add("editor", editorScreen);
        screens.add("movement.test", movementTestScreen);
        screens.add("inventory.test", inventoryTestScreen);

        setScreen("movement.test");
    }
}
