package com.silencestudios.escape.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.silencestudios.escape.components.TargetMoving;
import com.silencestudios.gdxengine.component.Transform;;

public class TargetMovingSystem extends IteratingSystem {

    public TargetMovingSystem() {
        super(Family.all(TargetMoving.class, Transform.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Transform transform = entity.getComponent(Transform.class);
        TargetMoving targetMoving = entity.getComponent(TargetMoving.class);

        if (targetMoving.target != null && transform.getPosition().dst2(targetMoving.target) > targetMoving.threshold) {
            Vector2 dir = new Vector2();
            dir.x = targetMoving.target.x - transform.getPosition().x;
            dir.y = targetMoving.target.y - transform.getPosition().y;
            dir.setLength(targetMoving.speed);

            transform.moveBy(dir.x * deltaTime, dir.y * deltaTime);
        } else if (targetMoving.target != null) {
            transform.moveTo(targetMoving.target);
            targetMoving.OnTargetReached.run();
        }

        Vector2 target = Vector2.X;
        target.setAngle(targetMoving.targetAngle);

        Vector2 source = Vector2.Y;
        source.setAngle(transform.getRotation());

        float dr = Math.signum(-target.angle(source)) * targetMoving.angularSpeed;
        if (Math.abs(target.angle(source)) > Math.abs(dr * deltaTime)) {
            transform.setRotation(transform.getRotation() + dr * deltaTime);
        } else {
            transform.setRotation(targetMoving.targetAngle);
        }
    }

    private float clampAngle(float angle) {
        if (angle > 360) {
            return angle - 360;
        }
        if (angle < 0) {
            return angle + 360;
        }

        return angle;
    }
}
