package com.silencestudios.escape.system;

public interface PausingSystem {
    void onPause();

    void onResume();
}
