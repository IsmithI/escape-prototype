package com.silencestudios.escape.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.silencestudios.escape.entity.player.PlayerTag;
import com.silencestudios.gdxengine.component.Transform;;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.entity.MainCamera;

public class CameraFollowSystem extends IteratingSystem {

    @Dependency(ID = "mainCamera")
    private MainCamera mainCamera;

    public CameraFollowSystem() {
        super(Family.all(PlayerTag.class).get());
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        engine.addEntityListener(getFamily(), new EntityListener() {
            @Override
            public void entityAdded(Entity player) {
                Transform cameraTransform = mainCamera.getComponent(Transform.class);
                Transform playerTransform = player.getComponent(Transform.class);

                cameraTransform.moveTo(playerTransform.getPosition());
            }

            @Override
            public void entityRemoved(Entity entity) {

            }
        });
    }

    @Override
    protected void processEntity(Entity player, float deltaTime) {
        Transform cameraTransform = mainCamera.getComponent(Transform.class);
        Transform playerTransform = player.getComponent(Transform.class);

        Vector2 distance = new Vector2(playerTransform.getPosition()).sub(cameraTransform.getPosition());
        float speed = distance.len() > 5f ? 5f : MathUtils.lerp(3f, 5f, distance.len() / 5f);

        Vector2 velocity = new Vector2(distance).setLength(speed * deltaTime);

        if (distance.len() <= velocity.len()) {
            cameraTransform.moveTo(playerTransform.getPosition());
            return;
        }

        cameraTransform.moveBy(velocity);
    }
}
