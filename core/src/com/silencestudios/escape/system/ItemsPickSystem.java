package com.silencestudios.escape.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.silencestudios.escape.components.DroppedItemData;
import com.silencestudios.escape.components.ItemsPicker;
import com.silencestudios.escape.components.TargetMoving;
import com.silencestudios.escape.entity.item.PickedItemEffect;
import com.silencestudios.gdxengine.component.SpriteRenderer;
import com.silencestudios.gdxengine.component.Transform;;
import com.silencestudios.gdxengine.instance.Instance;
import com.silencestudios.gdxengine.system.AbstractCollisionSystem;

public class ItemsPickSystem extends AbstractCollisionSystem {

    public ItemsPickSystem() {
        super(
                Family.all(ItemsPicker.class).get(),
                Family.all(DroppedItemData.class).get()
        );
    }

    @Override
    public void handleContactBegin(Entity itemPickerEntity, Entity item) {
        DroppedItemData itemData = item.getComponent(DroppedItemData.class);

        ItemsPicker itemsPicker = itemPickerEntity.getComponent(ItemsPicker.class);
        itemsPicker.OnItemPicked.accept(itemData);

        Entity effect = Instance.get().create(PickedItemEffect.class);
        effect.add(item.getComponent(SpriteRenderer.class));
        effect.getComponent(Transform.class).moveTo(item.getComponent(Transform.class).getPosition());
        effect.getComponent(TargetMoving.class).target = itemPickerEntity.getComponent(Transform.class).getPosition();

        Instance.get().remove(item);
    }

    @Override
    public void handleContactEnd(Entity itemPicker, Entity itemData) {

    }
}
