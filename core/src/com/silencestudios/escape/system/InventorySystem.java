package com.silencestudios.escape.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.silencestudios.escape.entity.ui.inventory.UIInventory;
import com.silencestudios.gdxengine.instance.Instance;

public class InventorySystem extends EntitySystem {

    @Override
    public void addedToEngine(Engine engine) {
        Instance.get().create(UIInventory.class);
    }
}
