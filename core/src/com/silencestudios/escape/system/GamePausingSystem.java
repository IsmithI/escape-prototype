package com.silencestudios.escape.system;

import com.badlogic.ashley.core.EntitySystem;
import com.silencestudios.gdxengine.component.EventListener;
import com.silencestudios.gdxengine.utils.Event;

public class GamePausingSystem extends EntitySystem implements EventListener<Boolean> {

    @Override
    public void onEvent(Event<Boolean> event) {
        for (EntitySystem system : getEngine().getSystems()) {
            if (system instanceof PausingSystem) {
                PausingSystem pausingSystem = (PausingSystem) system;
                if (event.data) {
                    pausingSystem.onPause();
                } else {
                    pausingSystem.onResume();
                }
            }
        }

    }

    @Override
    public String getEventType() {
        return "game.paused";
    }
}
