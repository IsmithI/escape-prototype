package com.silencestudios.escape.system.player.movement;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.silencestudios.escape.components.Movable;
import com.silencestudios.escape.entity.UITouchpad;
import com.silencestudios.escape.entity.player.PlayerTag;
import com.silencestudios.escape.system.PausingSystem;
import com.silencestudios.gdxengine.component.PhysicsBody;
import com.silencestudios.gdxengine.input.ActionKeys;
import com.silencestudios.gdxengine.input.Input;
import com.silencestudios.gdxengine.instance.Instance;

public class PlayerMovementSystem extends IteratingSystem implements PausingSystem {

    private Touchpad touchpad;
    private Vector2 velocity = new Vector2();
    private Vector2 direction = new Vector2();

    public PlayerMovementSystem() {
        super(Family.all(PlayerTag.class).get());
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        if (Gdx.app.getType().equals(Application.ApplicationType.Android)) {
            UITouchpad uiTouchpad = Instance.get().create(UITouchpad.class);
            touchpad = uiTouchpad.getTouchpad();
        }
    }

    @Override
    protected void processEntity(Entity player, float deltaTime) {
        if (!checkProcessing())
            return;

        Movable movable = player.getComponent(Movable.class);
        PhysicsBody physicsBody = player.getComponent(PhysicsBody.class);

        direction.setZero();

        if (Input.get().isActionPressed(ActionKeys.UP))
            direction.y = 1;
        if (Input.get().isActionPressed(ActionKeys.DOWN))
            direction.y = -1;
        if (Input.get().isActionPressed(ActionKeys.LEFT))
            direction.x = -1;
        if (Input.get().isActionPressed(ActionKeys.RIGHT))
            direction.x = 1;

        if (Gdx.app.getType().equals(Application.ApplicationType.Android)) {
            if (touchpad.isTouched()) {
                float knobPercentX = touchpad.getKnobPercentX();
                float knobPercentY = touchpad.getKnobPercentY();
                direction.set(knobPercentX, knobPercentY);
            }
        }

        if (!direction.isZero()) {
            velocity.set(direction)
                    .setLength(movable.speed);

            movable.velocity.set(velocity);
            movable.direction.set(direction);

            physicsBody.body.setLinearVelocity(velocity);
        } else {
            movable.velocity.setZero();
            physicsBody.body.setLinearVelocity(movable.velocity);
        }
    }

    @Override
    public void onPause() {
        setProcessing(false);
    }

    @Override
    public void onResume() {
        setProcessing(true);
    }
}
