package com.silencestudios.escape.system.player.animation;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.silencestudios.escape.components.Movable;
import com.silencestudios.escape.entity.player.PlayerSprite;
import com.silencestudios.escape.entity.player.PlayerTag;
import com.silencestudios.gdxengine.component.Animator;
import com.silencestudios.gdxengine.component.Transform;;

public class PlayerAnimationSystem extends IteratingSystem {

    public PlayerAnimationSystem() {
        super(Family.all(PlayerTag.class).get());
    }

    @Override
    protected void processEntity(Entity player, float deltaTime) {
        Movable movable = player.getComponent(Movable.class);
        Transform root = player.getComponent(Transform.class);
        PlayerSprite playerSprite = (PlayerSprite) root.findChild(PlayerSprite.class);

        Animator animator = playerSprite.getComponent(Animator.class);

        if (movable.velocity.isZero()) {
            animator.resetStateTime();
            animator.pause();
        } else {
            animator.resume();
            boolean horizontal = Math.abs(movable.velocity.x) > Math.abs(movable.velocity.y);
            if (movable.velocity.x > 0 && horizontal) {
                animator.setCurrentAnimation("run-right");
            }
            if (movable.velocity.x < 0 && horizontal) {
                animator.setCurrentAnimation("run-left");
            }
            if (movable.velocity.y < 0 && !horizontal) {
                animator.setCurrentAnimation("run-down");
            }
            if (movable.velocity.y > 0 && !horizontal) {
                animator.setCurrentAnimation("run-up");
            }
        }
    }
}
