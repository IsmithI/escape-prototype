package com.silencestudios.escape.system.player.combat;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.silencestudios.escape.components.Movable;
import com.silencestudios.escape.entity.player.PlayerTag;
import com.silencestudios.escape.entity.slash.Slash;
import com.silencestudios.gdxengine.component.Transform;
import com.silencestudios.gdxengine.instance.Instance;

;

public class PlayerCombatSystem extends EntitySystem {

    private Entity character;

    private Movable movable;
    private Transform transform;

    @Override
    public void addedToEngine(Engine engine) {
        engine.addEntityListener(Family.all(PlayerTag.class).get(), new EntityListener() {
            @Override
            public void entityAdded(Entity player) {
                character = player;
            }

            @Override
            public void entityRemoved(Entity entity) {
            }
        });
    }

    @Override
    public void update(float deltaTime) {
        if (!checkProcessing())
            return;

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            movable = character.getComponent(Movable.class);
            transform = character.getComponent(Transform.class);

            Instance.get().create(Slash.class, slash -> {
                Transform slashTransform = slash.getComponent(Transform.class);
                slashTransform.moveBy(movable.direction);

                transform.addChild(slash);
            });
        }
    }
}
