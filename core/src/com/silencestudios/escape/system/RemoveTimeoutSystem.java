package com.silencestudios.escape.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.silencestudios.escape.components.RemoveTimeout;
import com.silencestudios.gdxengine.instance.Instance;

public class RemoveTimeoutSystem extends IteratingSystem {

    public RemoveTimeoutSystem() {
        super(Family.all(RemoveTimeout.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        RemoveTimeout removeTimeout = entity.getComponent(RemoveTimeout.class);
        removeTimeout.timeout -= deltaTime;

        if (removeTimeout.timeout <= 0)
            Instance.get().remove(entity);
    }
}
