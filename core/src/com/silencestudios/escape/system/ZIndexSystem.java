package com.silencestudios.escape.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.silencestudios.gdxengine.component.Animator;
import com.silencestudios.gdxengine.component.SpriteRenderer;
import com.silencestudios.gdxengine.component.Transform;;

public class ZIndexSystem extends IteratingSystem {

    public ZIndexSystem() {
        super(Family.all(Transform.class).one(SpriteRenderer.class, Animator.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Transform transform = entity.getComponent(Transform.class);
        transform.setZ(-(transform.getPosition().y + transform.getOffset().y));
    }
}
