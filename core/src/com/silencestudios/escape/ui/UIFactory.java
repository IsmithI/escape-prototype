package com.silencestudios.escape.ui;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.silencestudios.gdxengine.component.annotations.Dependency;
import com.silencestudios.gdxengine.component.annotations.Resource;
import com.silencestudios.gdxengine.instance.Instance;

@Dependency
public class UIFactory extends Entity {

    private static UIFactory instance;

    public static UIFactory get() {
        if (instance == null) {
            instance = Instance.get().create(UIFactory.class);
        }
        return instance;
    }

    @Resource(key = "skin/skin.json")
    public Skin skin;

    public Button makeButton() {
        return new Button(skin);
    }

    public ImageButton makeImageButton(String image) {
        return new ImageButton(skin, image);
    }

    public TextButton makeTextButton(String text) {
        return new TextButton(text, skin);
    }

    public Label makeLabel(CharSequence text) {
        return new Label(text, skin);
    }

    public Label makeLabel(CharSequence text, String styleName) {
        return new Label(text, skin, styleName);
    }

    public Table makeTable() {
        return new Table(skin);
    }

    public Table makeTable(boolean withBackground) {
        Table table = new Table(skin);
        if (withBackground) {
            table.setBackground("pane-background");
        }
        return table;
    }

    public Touchpad makeTouchpad() {
        return new Touchpad(0f, skin);
    }
}
