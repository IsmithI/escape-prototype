package com.silencestudios.escape.ui;

import java.util.LinkedList;
import java.util.List;

public class ViewModel {

    private List<View> views = new LinkedList<>();

    public void addListener(View view) {
        views.add(view);
    }

    public void notifyListeners() {
        for (View view : views) {
            view.update(this);
        }
    }
}
