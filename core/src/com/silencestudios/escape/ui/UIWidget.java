package com.silencestudios.escape.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Observable;

public abstract class UIWidget<T extends Observable> {

    protected T model;

    public UIWidget() {
    }

    public UIWidget(T model) {
        this.model = model;
        this.model.addObserver((observable, o) -> this.update());
    }

    public abstract void update();

    public abstract Actor getActor();

    public T getModel() {
        return model;
    }
}
