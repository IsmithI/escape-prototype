package com.silencestudios.escape.ui;

import com.badlogic.ashley.core.Entity;

public abstract class View<T> extends Entity {
    public abstract void update(T model);
}
