package com.silencestudios.escape.components;

import com.badlogic.ashley.core.Component;

public class DroppedItemData implements Component {
    public String name;
    public String sprite;
    public int count = 1;
}
