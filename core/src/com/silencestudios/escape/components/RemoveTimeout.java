package com.silencestudios.escape.components;

import com.badlogic.ashley.core.Component;

public class RemoveTimeout implements Component {
    public float timeout = 1f;
}
