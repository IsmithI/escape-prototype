package com.silencestudios.escape.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class Movable implements Component {
    public float speed = 1f;
    public final Vector2 velocity = new Vector2();
    public final Vector2 direction = new Vector2(1, 0);
}
