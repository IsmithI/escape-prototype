package com.silencestudios.escape.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class TargetMoving implements Component {
    public float threshold = 0.2f;
    public float speed;
    public Vector2 target;
    public float targetAngle;
    public float angularSpeed;
    public float angularThreshold = 0.2f;
    public Runnable OnTargetReached = () -> {
    };
}
